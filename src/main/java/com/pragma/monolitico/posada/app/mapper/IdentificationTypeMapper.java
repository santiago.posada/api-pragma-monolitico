package com.pragma.monolitico.posada.app.mapper;


import com.pragma.monolitico.posada.app.dto.IdentificationTypeDTO;
import com.pragma.monolitico.posada.app.dto.PhotoDTO;
import com.pragma.monolitico.posada.app.entity.IdentificationType;
import com.pragma.monolitico.posada.app.entity.Photo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface IdentificationTypeMapper {


    IdentificationTypeMapper INSTANCE = Mappers.getMapper(IdentificationTypeMapper.class);


    public IdentificationType DTOtoEntity(IdentificationTypeDTO identificationTypeDTO);

    public IdentificationTypeDTO mapToDto(IdentificationType identificationType);

    public List<IdentificationTypeDTO> mapToDto(List<IdentificationType> identificationType);








}
