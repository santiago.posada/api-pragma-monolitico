package com.pragma.monolitico.posada.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PragmaMonoliticoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PragmaMonoliticoApplication.class, args);
	}

}
