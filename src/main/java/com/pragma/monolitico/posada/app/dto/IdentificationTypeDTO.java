package com.pragma.monolitico.posada.app.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentificationTypeDTO {




    private Long id;


    private String name;

    private String description;




}
