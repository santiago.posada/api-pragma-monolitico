package com.pragma.monolitico.posada.app.exceptions.client;

public class ClientDataMailExistException extends RuntimeException{

    public ClientDataMailExistException(String  mail) {
        super(String.format("The mail : %s is existing", mail));
    }


}
