package com.pragma.monolitico.posada.app.repository;

import com.pragma.monolitico.posada.app.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City,Long> {


}
