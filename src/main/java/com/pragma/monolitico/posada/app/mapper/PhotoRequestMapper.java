package com.pragma.monolitico.posada.app.mapper;

import com.pragma.monolitico.posada.app.dto.PhotoRequestDTO;
import com.pragma.monolitico.posada.app.entity.Photo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PhotoRequestMapper {

    PhotoRequestMapper INSTANCE = Mappers.getMapper(PhotoRequestMapper.class);

    public Photo DTOtoEntity(PhotoRequestDTO photoRequestDTO);

    public PhotoRequestDTO EntityToDTO(Photo photo);


}
