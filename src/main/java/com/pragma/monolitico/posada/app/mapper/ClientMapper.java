package com.pragma.monolitico.posada.app.mapper;

import com.pragma.monolitico.posada.app.dto.ClientDTO;
import com.pragma.monolitico.posada.app.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ClientMapper {

    ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

    public Client DTOtoEntity(ClientDTO clientDTO);

    public ClientDTO mapToDto(Client client);

    public List<ClientDTO> mapToDto(List<Client> clients);





}
