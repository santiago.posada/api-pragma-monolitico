package com.pragma.monolitico.posada.app.mapper;

import com.pragma.monolitico.posada.app.dto.CityDTO;
import com.pragma.monolitico.posada.app.dto.IdentificationTypeDTO;
import com.pragma.monolitico.posada.app.entity.City;
import com.pragma.monolitico.posada.app.entity.IdentificationType;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper
public interface CityMapper {


    CityMapper INSTANCE = Mappers.getMapper(CityMapper.class);


    public City DTOtoEntity(CityDTO cityDTO);

    public CityDTO mapToDto(City city);

    public List<IdentificationTypeDTO> mapToDto(List<City> cities);














}
