package com.pragma.monolitico.posada.app.data;

import com.pragma.monolitico.posada.app.entity.IdentificationType;

public class IdentificationTypeData {

    public static final IdentificationType identificationTypeCedula= IdentificationType.builder().id(1l).name("Cédula de Ciudadania").build();
    public static final IdentificationType identificationTypeIdentidad= IdentificationType.builder().id(2l).name("Tarjeta de Identidad").build();
}
